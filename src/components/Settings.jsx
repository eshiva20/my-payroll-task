import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Settings = ({ setSelectedTitle }) => {
  const navigate = useNavigate();
  useEffect(() => {
    setSelectedTitle("Settings");
    if (!localStorage.getItem("token")) {
      navigate("/login");
    }
  }, []);
  return <div>Settings</div>;

}

export default Settings
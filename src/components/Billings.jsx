import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Billings = ({ setSelectedTitle }) => {
  const navigate = useNavigate();
  useEffect(() => {
    setSelectedTitle("Billing");
    if (!localStorage.getItem("token")) {
      navigate("/login");
    }
  }, []);
  return <div>Billings</div>;
};

export default Billings;

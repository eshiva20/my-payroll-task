import { TextField, MenuItem } from "@mui/material";
import { useField } from "formik";
import React from "react";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    },
  },
};

const Priority = ({ name, setFun, control, ...otherProps }) => {
  const [field, meta] = useField(name);

  const configText = {
    ...otherProps,
    ...field,
  };

  if (meta && meta.touched && meta.error) {
    configText.error = true;
    configText.helperText = meta.error;
  }

  const defaultProps = {
    options: otherProps.data,
    getOptionLabel: (option) => option.LeadName,
  };

  const Menu = (
    <TextField
      className="menu_priority"
      select
      {...configText}
      size="small"
      fullWidth
      variant="standard"
      required
    >
      <MenuItem value="High">High Priority</MenuItem>
      <MenuItem value="Low">Low Priority</MenuItem>
    </TextField>
  );

  const Lead = (
    <TextField
      select
      {...configText}
      size="small"
      fullWidth
      variant="standard"
      MenuProps={MenuProps}
      required
    >
      {otherProps.data?.map((eachData) => (
        <MenuItem style={{ width: "300px" }} value={eachData.Id}>
          {eachData.LeadName}
        </MenuItem>
      ))}
    </TextField>

    // <Autocomplete
    //   {...defaultProps}
    //   renderInput={(params) => (
    //     <TextField  {...params} label="Lead/Customer Name" variant="standard" />
    //   )}
    // />
  );

  if (otherProps.checkFor === "LeadId") return <>{Lead}</>;
  else return <>{Menu}</>;
};

export default Priority;

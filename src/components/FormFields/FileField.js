import { TextField } from "@mui/material";
import { FastField } from "formik";
import React from "react";

const FileField = ({ setFun, handleMultimedia, setFun1, setIsFileErr }) => {
  const handleFile = async (e) => {
    if (e.target.files[0]?.size < 2000000) {
      setIsFileErr(false);
      const file = e.target.files[0];
      const newAr = e.target.files[0].name.split(".");
      const Extention = newAr[newAr.length - 1];
      const MultimediaType = Extention === "pdf" ? "D" : "I";
      const filsplice = newAr.slice(0, newAr.length - 1);
      const FileName = filsplice.join("");
      const baseString = await convertFileBase(file);
      handleMultimedia(baseString, FileName, Extention, MultimediaType);
      setFun1(e.target.files[0].name);
      setFun(e.target.files[0].name);
    } else {
      setIsFileErr(true);
    }
  };

  const convertFileBase = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  return (
    <div>
      <FastField name="MultimediaData" className="">
        {(props) => {
          return (
            <div>
              <TextField
                className="form_file"
                required
                type="file"
                fullWidth
                variant="standard"
                label="Attach File"
                onChange={(e) => {
                  handleFile(e);
                }}
              />
            </div>
          );
        }}
      </FastField>
    </div>
  );
};

export default FileField;

import { TextField } from "@mui/material";
import { useField } from "formik";
import React, { useEffect, useState } from "react";
import SelectUser from "../Modal/SelectUser";
import { useDispatch } from "react-redux";
import { getCompMembers } from "../../redux/companyMemberSlice";

const TextSelect = ({ name, setFun, ...otherProps }) => {
  const [field, meta] = useField(name);
  const [search, setSearch] = useState("");
  const configText = {
    ...otherProps,
    ...field,
    // fullWidth: true,
    variant: "standard",
  };

  const dispatch = useDispatch();

  if (meta && meta.touched && meta.error) {
    configText.error = true;
    configText.helperText = meta.error;
  }

  useEffect(() => {
    const isFiltered = setTimeout(() => {
      dispatch(getCompMembers(search));
    }, 1000);
    return () => {
      clearTimeout(isFiltered);
    };
  }, [search]);

  return (
    <div>
      <TextField
        required
        InputProps={{
          readOnly: true,
        }}
        {...configText}
        // onChange={() => setFun()}
        onClick={() => otherProps.setOpenAddUser(true)}
      />

      {otherProps.openAddUser && (
        <SelectUser
          setSearch={setSearch}
          search={search}
          userName="userCc"
          users={otherProps.users}
          usersIdObj={otherProps.usersIdObj}
          setUsersIdObj={(data) => otherProps.setUsersIdObj(data)}
          setUsersCc={otherProps.setUsersCc}
          openAddUser={otherProps.openAddUser}
          setUserCount={(data) => otherProps.setUserCount(data)}
          setOpenAddUser={(data) => otherProps.setOpenAddUser(data)}
        />
      )}
    </div>
  );
};

export default TextSelect;

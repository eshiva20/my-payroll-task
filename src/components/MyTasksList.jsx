import {
  CircularProgress,
  FormControl,
  IconButton,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
} from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
  setDifference,
  setEndFromAndTo,
  setFromAndTo,
  setSortData,
  setStartFromAndTo,
} from "../redux/myTasksSlice";
import { postStatus } from "../redux/postStatusUpdateSlice";
import AddTask from "./Modal/AddTask";
import PartialModal from "./Modal/PartialModal";
import "../style/pages.css";
import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import { getDisplayDate, isOverDue } from "../utility";
import { coverageTask } from "../redux/taskCoverageSlice";
import CoverageModal from "./Modal/CoverageModal";
import {
  ARCHIVETASK,
  DELETE_TASK,
  POST_UPDATE_TASK,
} from "../api/apiEndPoints";
import { RequestAPi } from "../api/Request";
import { toast } from "react-toastify";
import ConfirmModal from "./Modal/ConfirmModal";

const MyTasksList = ({ getData }) => {
  const [createDateOrder, setCreateDateOrder] = useState("1");
  const [dueDateOrder, setDueDateOrder] = useState("1");
  const [openDialogue, setOpenDialogue] = useState(false);
  const [openPartialModal, setOpenPartialModal] = useState(false);
  const [openCoverageModal, setOpenCoverageModal] = useState(false);
  const [open, setOpen] = useState(false);

  const [partialConfig, setPartialConfig] = useState({
    TaskId: 0,
    TaskStatusValue: 0,
  });

  const [confirmText, setConfirmText] = useState({
    heading: "",
    buttonText: "",
    handleClick: null,
  });

  const handleClickObj = {
    archieve: async (taskId) => {
      const res = await RequestAPi.post(ARCHIVETASK, {
        TaskId: taskId,
        IsArchive: true,
      });
      if (res) {
        toast.success("Task has been archived");
        setOpen(false);
        getData();
      }
    },
    deleteTask: async (taskId) => {
      const res = await RequestAPi.get(`${DELETE_TASK}?taskId=${taskId}`);
      if (res) {
        toast.success("Task deleted successfully");
        setOpen(false);
        getData();
      }
    },
    complete: async (taskId) => {
      const res = await RequestAPi.post(POST_UPDATE_TASK, {
        TaskId: taskId,
        TaskStatusValue: 100,
      });
      if (res) {
        toast.success("Task Completed Successfully");
        getData();
        setOpen(false);
      }
    },
  };

  const handleButtonClick = (str, id) => {
    setOpen(true);
    switch (str) {
      case "delete":
        setConfirmText({
          heading: "Delete",
          handleClick: () => handleClickObj.deleteTask(id),
        });
        return;
      case "archive":
        setConfirmText({
          heading: "Archive",
          handleClick: () => handleClickObj.archieve(id),
        });
        return;
      case "complete":
        setConfirmText({
          heading: "Complete",
          buttonText: "Yes",
          handleClick: () => handleClickObj.complete(id),
        });
        return;
      default:
        setOpen(false);
        return;
    }
  };

  const {
    localData,
    count,
    sendData,
    difference,
    toToDisplay,
    isPaginationLoading,
    isListFetching,
  } = useSelector((state) => state.mytasksReducer);

  const dispatch = useDispatch();

  const handlePartial = (id, percentage) => {
    const obj = { TaskId: id, TaskStatusValue: percentage };
    setPartialConfig(obj);
    setOpenPartialModal(true);
  };

  const handleCreateDate = (value) => {
    setCreateDateOrder(value);
    value !== "1" && setDueDateOrder("1");
    if (value === "2") {
      dispatch(setSortData({ order: "desc", column: "CreateDate" }));
    }
    if (value === "3") {
      dispatch(setSortData({ order: "asc", column: "CreateDate" }));
    }
  };

  const handleDueDate = (value) => {
    value !== "1" && setCreateDateOrder("1");
    setDueDateOrder(value);

    if (value === "2") {
      dispatch(setSortData({ order: "desc", column: "DueDate" }));
    }
    if (value === "3") {
      dispatch(setSortData({ order: "asc", column: "DueDate" }));
    }
  };

  const handleTaskCoverage = (id) => {
    dispatch(coverageTask({ TaskId: id }));
  };

  return (
    <div>
      <TableContainer
        className="task-table-container"
        component={Paper}
        style={{
          position: "relative",
          top: "-33px",
        }}
      >
        <Table size="small">
          <TableHead>
            <TableRow className="tasks_table_head_row">
              <TableCell align="left">
                <span className="cell">Title</span>
              </TableCell>
              <TableCell>
                <span className="cell">Customer Name</span>
              </TableCell>
              <TableCell>
                <span className="cell">Assigned By</span>
              </TableCell>
              <TableCell>
                <span className="cell">Assigned Date</span>{" "}
                {createDateOrder === "1" ? (
                  <IconButton onClick={() => handleCreateDate("2")}>
                    <img
                      alt="icon"
                      height="14px"
                      src=" https://testffc.nimapinfotech.com/assets/media/icons/sort.svg"
                    />
                  </IconButton>
                ) : createDateOrder === "2" ? (
                  <IconButton onClick={() => handleCreateDate("3")}>
                    <img
                      alt="icon"
                      height="14px"
                      src="https://testffc.nimapinfotech.com/assets/media/icons/downarrow.svg"
                    />
                  </IconButton>
                ) : createDateOrder === "3" ? (
                  <IconButton onClick={() => handleCreateDate("2")}>
                    <img
                      alt="icon"
                      height="14px"
                      src="https://testffc.nimapinfotech.com/assets/media/icons/uparrow.svg"
                    />
                  </IconButton>
                ) : (
                  ""
                )}
              </TableCell>
              <TableCell>
                <span className="cell">Due Date</span> {}{" "}
                {dueDateOrder === "1" ? (
                  <IconButton onClick={() => handleDueDate("2")}>
                    <img
                      alt="icon"
                      height="14px"
                      src=" https://testffc.nimapinfotech.com/assets/media/icons/sort.svg"
                    />
                  </IconButton>
                ) : dueDateOrder === "2" ? (
                  <IconButton onClick={() => handleDueDate("3")}>
                    <img
                      alt="icon"
                      height="14px"
                      src="https://testffc.nimapinfotech.com/assets/media/icons/downarrow.svg"
                    />
                  </IconButton>
                ) : dueDateOrder === "3" ? (
                  <IconButton onClick={() => handleDueDate("2")}>
                    <img
                      alt="icon"
                      height="14px"
                      src="https://testffc.nimapinfotech.com/assets/media/icons/uparrow.svg"
                    />
                  </IconButton>
                ) : (
                  ""
                )}
              </TableCell>
              <TableCell>
                <span className="cell">Priority</span>
              </TableCell>
              <TableCell>
                <span className="cell ">Status</span>
              </TableCell>
              <TableCell className="cell "></TableCell>
              <TableCell className="cell "></TableCell>
              <TableCell className="cell "></TableCell>
              <TableCell className="cell "></TableCell>
              <TableCell className="cell "></TableCell>
              <TableCell className="cell "></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!isListFetching ? (
              localData?.length !== 0 ? (
                localData?.map((data) => (
                  <TableRow className="table_row_body">
                    <TableCell sx={{ maxWidth: 110 }} className="tab_cell">
                      <span className="cell span-link">{data.Title}</span>
                    </TableCell>

                    <TableCell className="tab_cell">
                      <span className="cell span-link">
                        {data.LeadName ? data.LeadName : "-"}
                      </span>
                    </TableCell>

                    <TableCell className="tab_cell" padding="none">
                      <span className="cell">{data.AssignedByUserName}</span>
                    </TableCell>

                    <TableCell className="tab_cell">
                      <span className="cell">
                        {getDisplayDate(data.CreateDate)}
                      </span>
                    </TableCell>

                    <TableCell className="tab_cell">
                      <span className="cell">
                        {getDisplayDate(data.TaskEndDate)}
                      </span>
                      {isOverDue(data.TaskEndDate) && (
                        <Tooltip title="Overdue" placement="bottom" arrow>
                          <img
                            alt="icon"
                            style={{ marginLeft: "2px" }}
                            height="15px"
                            src="https://testffc.nimapinfotech.com/assets/media/icons/overdue.svg"
                          />
                        </Tooltip>
                      )}
                    </TableCell>

                    <TableCell className="tab_cell">
                      <span className="cell">{data.Priority}</span>
                    </TableCell>

                    <TableCell className="tab_cell">
                      {data.TaskStatus > 0 && data.TaskStatus < 100 ? (
                        <span className="cell status-partial">
                          {`Partial Complete (${data.TaskStatus}%)`}{" "}
                        </span>
                      ) : data.TaskStatus === 0 ? (
                        <span className="cell status-accepted">Accepted</span>
                      ) : data.TaskStatus === -1 ? (
                        <span className="cell status-not-accepted">
                          Not Accepted
                        </span>
                      ) : data.TaskStatus === 100 ? (
                        <span className="cell status-completed">Completed</span>
                      ) : (
                        ""
                      )}
                    </TableCell>

                    <TableCell
                      component="td"
                      className="cell"
                      sx={{ maxWidth: 40 }}
                    >
                      {data.AssignedByUserName === "Rijo Varghese" ? (
                        <Tooltip title="Taskarchieve" placement="bottom" arrow>
                          <IconButton
                            onClick={() =>
                              handleButtonClick("archive", data.TaskId)
                            }
                          >
                            <img
                              alt="icon"
                              height="20px"
                              src="https://testffc.nimapinfotech.com/assets/media/task/TaskArchive.svg"
                            />
                          </IconButton>
                        </Tooltip>
                      ) : (
                        ""
                      )}
                    </TableCell>

                    <TableCell
                      component="td"
                      className="cell"
                      sx={{ maxWidth: 40 }}
                    >
                      {data.TaskStatus === -1 && (
                        <Tooltip title="Accept" placement="bottom" arrow>
                          <IconButton
                            onClick={() =>
                              dispatch(
                                postStatus({
                                  data: {
                                    TaskId: data.TaskId,
                                    TaskStatusValue: 0,
                                  },
                                })
                              )
                            }
                          >
                            <img
                              alt="icon"
                              height="20px"
                              src="https://testffc.nimapinfotech.com/assets/media/task/TaskAccept.svg"
                            />
                          </IconButton>
                        </Tooltip>
                      )}
                    </TableCell>

                    <TableCell
                      component="td"
                      className="cell"
                      sx={{ maxWidth: 40 }}
                    >
                      {data.AssignedByUserName === "Rijo Varghese" ? (
                        <Tooltip
                          title="View Task Coverage"
                          placement="bottom"
                          arrow
                        >
                          <IconButton
                            onClick={() => {
                              handleTaskCoverage(data.TaskId);
                              setOpenCoverageModal(true);
                            }}
                          >
                            <img
                              alt="icon"
                              height="20px"
                              src="https://testffc.nimapinfotech.com/assets/media/task/TaskViewTaskCoverage.svg"
                            />
                          </IconButton>
                        </Tooltip>
                      ) : (
                        ""
                      )}
                    </TableCell>

                    <TableCell
                      component="td"
                      className="cell"
                      sx={{ maxWidth: 40 }}
                    >
                      {data.AssignedByUserName === "Rijo Varghese" ? (
                        <Tooltip title="Delete" placement="bottom" arrow>
                          <IconButton
                            onClick={() =>
                              handleButtonClick("delete", data.TaskId)
                            }
                          >
                            <img
                              alt="icon"
                              height="20px"
                              src="https://testffc.nimapinfotech.com/assets/media/task/TaskDelete.svg"
                            />
                          </IconButton>
                        </Tooltip>
                      ) : (
                        ""
                      )}
                    </TableCell>

                    <TableCell
                      component="td"
                      className="cell"
                      sx={{ maxWidth: 40 }}
                    >
                      {data.TaskStatus >= 0 && data.TaskStatus < 100 && (
                        <Tooltip title="Complete" placement="bottom" arrow>
                          <IconButton
                            onClick={() =>
                              handleButtonClick("complete", data.TaskId)
                            }
                          >
                            <img
                              alt="icon"
                              height="20px"
                              src="https://testffc.nimapinfotech.com/assets/media/task/TaskComplete.svg"
                            />
                          </IconButton>
                        </Tooltip>
                      )}
                    </TableCell>

                    <TableCell
                      component="td"
                      className="cell"
                      sx={{ maxWidth: 30 }}
                    >
                      {data.TaskStatus >= 0 && data.TaskStatus < 100 && (
                        <Tooltip
                          title="Partial Complete"
                          placement="bottom"
                          arrow
                        >
                          <IconButton
                            onClick={() =>
                              handlePartial(data.TaskId, data.TaskStatus)
                            }
                          >
                            <img
                              alt="icon"
                              height="20px"
                              src="https://testffc.nimapinfotech.com/assets/media/task/TaskPartialComplete.svg"
                            />
                          </IconButton>
                        </Tooltip>
                      )}
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan={17} style={{ textAlign: "center" }}>
                    No Records Found
                  </TableCell>
                </TableRow>
              )
            ) : (
              <TableRow>
                <TableCell colSpan={17} style={{ textAlign: "center" }}>
                  Fetching List Please Wait.....{" "}
                  <CircularProgress style={{ height: "20px", width: "22px" }} />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <AddTask open={openDialogue} setOpen={(data) => setOpenDialogue(data)} />

      {openPartialModal && (
        <PartialModal
          config={partialConfig}
          open={openPartialModal}
          setOpen={(data) => setOpenPartialModal(data)}
        />
      )}

      {openCoverageModal && (
        <CoverageModal
          openCoverageModal={openCoverageModal}
          setOpenCoverageModal={setOpenCoverageModal}
        />
      )}

      {open && (
        <ConfirmModal
          open={open}
          setOpen={setOpen}
          heading={confirmText.heading}
          buttonText={confirmText.buttonText}
          handleClick={confirmText.handleClick}
        />
      )}

      <div className="pagination">
        <div>
          <Tooltip title="First Page" placement="top" arrow>
            <IconButton
              onClick={() => dispatch(setStartFromAndTo())}
              disabled={sendData.From === 1}
            >
              <span className="icon-button-style">{`|`}</span>
              <NavigateBeforeIcon fontSize="medium" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Previous Page" placement="top" arrow>
            <IconButton
              disabled={sendData.From === 1}
              onClick={() => dispatch(setFromAndTo({ direction: "rtl" }))}
            >
              <NavigateBeforeIcon fontSize="medium" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Next Page" placement="top" arrow>
            <IconButton
              disabled={sendData.To >= count}
              onClick={() => dispatch(setFromAndTo({ direction: "ltr" }))}
            >
              <NavigateNextIcon fontSize="medium" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Last Page" placement="top" arrow>
            <IconButton
              onClick={() => dispatch(setEndFromAndTo())}
              disabled={sendData.To >= count}
            >
              <NavigateNextIcon fontSize="medium" />
              <span className="icon-button-style">{`|`}</span>
            </IconButton>
          </Tooltip>
        </div>
        <div className="item-page">
          <span>
            {sendData.From}-{sendData.To <= count ? sendData.To : count} of{" "}
            {count}
          </span>
        </div>
        <div className="nav-menu">
          <FormControl
            variant="standard"
            size="small"
            sx={{ pt: 1, mr: 2, minWidth: 40, fontStretch: 1 }}
          >
            <Select
              sx={{ fontSize: "12px", fontWeight: "530" }}
              value={difference}
              onChange={(e) => dispatch(setDifference(e.target.value))}
            >
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={15}>15</MenuItem>
              <MenuItem value={25}>25</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div className="item-page">
          <span>Items Per Pages: </span>
        </div>{" "}
        {isPaginationLoading && (
          <div id="tablePagination">
            <CircularProgress
              style={{
                marginTop: "10px",
                marginRight: "5px",
                height: "16px",
                width: "16px",
              }}
              color="inherit"
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default MyTasksList;

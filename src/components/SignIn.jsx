import { Formik, Form, Field, ErrorMessage } from "formik";
import { Button, TextField } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { signInUser } from "../redux/signInSlice";
import { object, string } from "yup";
import "../style/pages.css";

const numReg = /^[0-9]{0,11}$/;

const SignIn = () => {
  const navigate = useNavigate();
  const { loading } = useSelector((state) => state.signInReducer);
  const dispatch = useDispatch();

  const initialValues = {
    Username: "",
    Password: "",
  };

  const validationSchema = object().shape({
    Username: string()
      .required("Username cannot be empty")
      .matches(numReg, "Username must contain only numbers")
      .max(10, "Username length must be less than or equal to 10"),
    Password: string()
      .required("Password cannot be empty")
      .min(6, "Password length must be more than 6"),
  });

  const handleSubmit = (values) => {
    dispatch(signInUser({ data: values, navigate }));
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      navigate("/dashboard");
    } else {
      navigate("/login");
    }
  }, []);

  return (
    <div className="login-main">
      <div className="left-panel"></div>
      <div className="login-content">
        <div className="signup">
          <span className="signHead">Don't have an account yet? </span>
          <span className="signlink">Sign Up</span>
        </div>

        <div>
          <div className="login_containt">
            <div className="logo-head">
              <div className="logo">
                <img
                  src="https://testffc.nimapinfotech.com/image/New%20Images/FFC/FFC-logo.png"
                  alt="logo"
                />
              </div>
              Get Started with BETA Field Force
            </div>

            <div className="login">
              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={handleSubmit}
              >
                <Form>
                  <div className="login_text_div">
                    <Field
                      as={TextField}
                      fullWidth
                      label="Email id / Mobile No"
                      variant="standard"
                      type="text"
                      name="Username"
                      size="small"
                    />
                    <ErrorMessage
                      name="Username"
                      component="span"
                      className="error_msg"
                    />
                  </div>
                  <div className="login_text_div">
                    <Field
                      as={TextField}
                      fullWidth
                      label="Password"
                      variant="standard"
                      type="password"
                      name="Password"
                    />
                    <ErrorMessage
                      name="Password"
                      component="span"
                      className="error_msg"
                    />
                  </div>
                  <div className="login_buttons">
                    <Button
                      size="small"
                      color="primary"
                      className="loginform-btns"
                    >
                      Forgot Password
                    </Button>
                    <Button
                      size="small"
                      variant="contained"
                      color="primary"
                      type="submit"
                      className="loginform-btns"
                      disabled={loading}
                    >
                      Sign In
                    </Button>
                    OR
                    <Button
                      size="small"
                      variant="contained"
                      color="primary"
                      className="loginform-btns"
                    >
                      Sign In With OTP
                    </Button>
                  </div>
                </Form>
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;

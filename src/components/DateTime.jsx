import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import "../style/pages.css";
import "./test.css";

const DateTime = () => {
  const [min1, setMin1] = useState("");
  const [hour1, setHour1] = useState("");
  const [date1, setDate1] = useState("");
  const [month1, setMonth1] = useState("");
  const [year1, setYear1] = useState("");

  useEffect(() => {
    const interval = setInterval(() => {
      const today = new Date();
      setMin1(today.getMinutes());
      setHour1(today.getHours());
      setDate1(today.getDate());
      setMonth1(RetriveMonth(today.getMonth()));
      setYear1(today.getFullYear());
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const RetriveMonth = (m) => {
    const arr = [
      "Jan",
      "Feb",
      "Mar",
      "April",
      "May",
      "June",
      "July",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    return arr[m];
  };

  const showTime = () => {
    let hr = 0;
    let format = "AM";

    if (hour1 > 12) {
      hr = hour1 - 12;
      format = "PM";
    } else {
      hr = hour1;
      format = "AM";
    }
    return `${hr}:${min1 < 10 ? `0${min1}` : min1} ${format}`;
  };

  if(!min1){
    return
  }

  return (
    <div className="Time">
      <div>
        <Button color="primary" variant="contained" size="small">
          Punch In
        </Button>
      </div>
      <div className="time-div">{`${month1} ${date1}, ${year1}`}</div>
      <h4 className="disp-time">|</h4>
      <div size="small" className="disp-day">
        {showTime()}
      </div>
    </div>
  );
};

export default DateTime;

import * as React from "react";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import "./modal.css";
import { useSelector } from "react-redux";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function CoverageModal({
  openCoverageModal,
  setOpenCoverageModal,
}) {
  const handleClose = () => {
    setOpenCoverageModal(false);
  };

  const { coverageData } = useSelector((state) => state.coverageTaskReducer);

  return (
    <div>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openCoverageModal}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Task Coverage
        </BootstrapDialogTitle>
        <DialogContent dividers>
          {coverageData !== "" ? (
            <div className="coverage-div">
              <span className="coverage-content">
                Accepted <span>{coverageData?.Accepted}%</span>
              </span>
              <span className="coverage-content">
                Completed <span>{coverageData?.Completed}%</span>
              </span>
              <span className="coverage-content">
                Not Accepted <span>{coverageData?.["Not Started"]}%</span>
              </span>
              <span className="coverage-content">
                Partial Compelete <span>{coverageData?.Pending}%</span>
              </span>
            </div>
          ) : (
            ""
          )}
        </DialogContent>
      </BootstrapDialog>
    </div>
  );
}

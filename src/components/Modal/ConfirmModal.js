import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from "@mui/material";
// import classes from "./MyTask.module.css"

const ConfirmModal = ({
  open,
  setOpen,
  heading,
  handleClick,
  buttonText
}) => {
  return (
    <Dialog open={open}>
      {/* {(isDeleting || isPostLoading) && (
        <div>
          {" "}
          <LinearProgress />
        </div>
      )} */}
      <DialogTitle>
        {heading}
      </DialogTitle>
      <DialogContent dividers>
        <DialogContentText>
          <div >{`Are you sure you want to ${heading.toLowerCase()} this task?`}</div>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => setOpen(false)}
          size="small"
          variant="text"
          sx={{ height: 27, fontSize: 13, padding: 1 }}
        >
          <Typography
            sx={{ fontSize: 14, textTransform: "capitalize", color: "black" }}
          >
            Cancel
          </Typography>
        </Button>
        <Button
          size="small"
          variant="text"
          color="primary"
          onClick={handleClick}
          sx={{ height: 27, fontSize: 13, padding: 1, m: 1, mr: 3 }}
        >
          <Typography sx={{ fontSize: 14, textTransform: "capitalize" }}>
            {buttonText || heading}
          </Typography>
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmModal;
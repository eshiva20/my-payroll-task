import { LoadingButton, TabContext, TabList, TabPanel } from "@mui/lab";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Tab,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTask } from "../../redux/postStatusUpdateSlice";
import { sendDate } from "../../utility";
import AssignOthers from "./AssignOthers";
import "./modal.css";
import SaveIcon from "@mui/icons-material/Save";
import { getMyTasks } from "../../redux/myTasksSlice";

const AddTask = ({ open, setOpen }) => {
  const { isLoading } = useSelector((state) => state.postStatusUpdateReducer);
  const {sendData } = useSelector((state) => state.mytasksReducer);

  const submitObj = {
    Id: "",
    AssignedToUserId: "",
    AssignedDate: "",
    CompletedDate: "",
    IntercomGroupIds: [],
    IsActive: true,
    Latitude: "",
    Location: "",
    Longitude: "",
    TaskStatus: "",
  };

  const dispatch = useDispatch();
  const formRef = useRef();
  const [value, setValue] = useState("1");

  const handleChange = (e, nextValue) => {
    setValue(nextValue);
  };

  const handle = () => {
    formRef.current.submitForm();
  };

  const handleSubmit = async (values, MediaObject, usersObj) => {
    const TaskEndDate = sendDate(values.TaskEndDate, "TaskEndDate");

    let obj = submitObj;

    obj.Priority = values.Priority;
    obj.Description = values.Description;
    obj.Title = values.Title;
    obj.TaskOwners = usersObj;
    obj.TaskEndDate = TaskEndDate;
    obj.MultimediaData = MediaObject.MultimediaData;
    obj.MultimediaExtension = MediaObject.MultimediaExtension;
    obj.MultimediaFileName = MediaObject.MultimediaFileName;
    obj.MultimediaType = MediaObject.MultimediaType;
    obj.AssignedBy = Number(localStorage.getItem("userId"));
    obj.UserIds = [obj.AssignedBy];
    obj.LeadId = values.LeadId;

    const response = await dispatch(addTask({ data: obj }));
    if (response.error === undefined) {
      dispatch(getMyTasks(sendData))
      setOpen(false);
    } else {
      setOpen(false);
    }
  };

  return (
    <div className="add-main">
      <Dialog open={open}>
        <DialogTitle className="d-title">Add Task</DialogTitle>
        <DialogContent className="add-d-content" dividers>
          <DialogContentText>
            <Box>
              <TabContext value={value}>
                <Box className="add-d-box">
                  <TabList
                    className="add-t-list"
                    onChange={handleChange}
                    textColor="black"
                    indicatorColor="primary"
                  >
                    <Tab
                      className="add-tab-1"
                      label="Assigned to Others"
                      value="1"
                    />
                    <Tab
                      className="add-tab-2"
                      label="Assigned to Me"
                      value="2"
                    />
                  </TabList>
                </Box>
                <TabPanel sx={{ padding: "3px" }} value="1">
                  <AssignOthers
                    handleSubmit={(values, MediaObject, a) => {
                      handleSubmit(values, MediaObject, a);
                    }}
                    ref={formRef}
                  />
                </TabPanel>
                <TabPanel value="2">{"Some Text Here"}</TabPanel>
              </TabContext>
            </Box>
          </DialogContentText>
        </DialogContent>

        <DialogActions>
          <Button
            variant="contained"
            onClick={() => setOpen(false)}
            sx={{ height: 27, fontSize: 13, padding: 1 }}
          >
            Cancel
          </Button>
          <LoadingButton
            size="small"
            onClick={() => handle()}
            loading={isLoading}
            variant="contained"
            color="primary"
            type="submit"
            loadingPosition="end"
            sx={{ height: 27, fontSize: 13, padding: 1 }}
            endIcon={isLoading ? <SaveIcon /> : ""}
          >
            Add
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddTask;

import { Button } from "@mui/material";
import { Formik, Form } from "formik";
import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { getLeads } from "../../redux/leadsSlice";
import * as Yup from "yup";
import FormControl from "../FormControl";
import "./modal.css";
const titleReg = /^[A-za-z]{0,25}$/;

const initialState = {
  Title: "",
  Address: "",
  Description: "",
  Multimedia: "",
  LeadId: "",
  TaskEndDate: "",
  Priority: "",
  UserIds: "",
};

const form_validation = Yup.object().shape({
  Title: Yup.string().required("Required").matches(titleReg, "Invalid title"),
  Description: Yup.string().required("Required"),
  Address: Yup.mixed().required("Required"),
  LeadId: Yup.string().required("Required"),
  TaskOwners: Yup.string().required("Required"),
  TaskEndDate: Yup.string().required("Required"),
  Priority: Yup.string().required("Required"),
});

const AssignOthers = ({ handleSubmit }, ref) => {
  const { localLeadData } = useSelector((state) => state.leadsReducer);

  useImperativeHandle(
    ref,
    () => {
      return {
        submitForm: () => {
          document.getElementById("Add").click();
        },
      };
    },
    []
  );

  const [usersCc, setUsersCc] = useState([]);
  const [usersIdObj, setUsersIdObj] = useState([]);

  const [MediaObject, setMediaObject] = useState({
    MultimediaData: "",
    MultimediaFileName: "",
    MultimediaExtension: "",
    MultimediaType: "",
  });

  const dispatch = useDispatch();
  const [openAddCc, setOpenAddCc] = useState(false);
  const [isFileErr, setIsFileErr] = useState(false);

  const handleMultimedia = (
    baseString,
    mediaName,
    mediaExtention,
    MultimediaType
  ) => {
    setMediaObject({
      MultimediaData: baseString,
      MultimediaFileName: mediaName,
      MultimediaExtension: mediaExtention,
      MultimediaType: MultimediaType,
    });
  };

  const handleSubmitLocal = (values) => {
    handleSubmit(values, MediaObject, usersCc);
  };

  useEffect(() => {
    dispatch(getLeads());
  }, []);

  return (
    <div className="forr">
      <Formik
        initialValues={initialState}
        validationSchema={form_validation}
        onSubmit={handleSubmitLocal}
      >
        {(formik) => {
          return (
            <Form className="new">
              <div className="ani">
                <FormControl
                  control="input"
                  className="titleName"
                  name="Title"
                  label="Title"
                  setFun={(data) => formik.setFieldValue("Title", data)}
                />
                <FormControl
                  className="descriptionName"
                  control="input"
                  name="Description"
                  label="Description"
                  setFun={(data) => formik.setFieldValue("Description", data)}
                />
                <div className="fileSelect">
                  <FormControl
                    control="file"
                    name="Multimedia"
                    label="Attach File"
                    setFun={(data) => formik.setFieldValue("Multimedia", data)}
                    setFun1={(data) => formik.setFieldValue("Address", data)}
                    isFileErr={isFileErr}
                    setIsFileErr={setIsFileErr}
                    handleMultimedia={(data, name, extention, MultimediaType) =>
                      handleMultimedia(data, name, extention, MultimediaType)
                    }
                  />
                </div>

                <div className="fileName">
                  <FormControl
                    control="Address"
                    name="Address"
                    label="Attach File"
                    value={formik.values.Address}
                    setFun={() => formik.setFieldValue("Address", "")}
                    setFun2={() => formik.setFieldValue("Multimedia", "")}
                  />
                </div>
                {isFileErr && (
                  <span className="file-errmsg">
                    File must be less than 2MB
                  </span>
                )}

                <FormControl
                  control="Priority"
                  checkFor="LeadId"
                  name="LeadId"
                  label="Lead/Customer Name"
                  data={localLeadData}
                  setFun={(data) => formik.setFieldValue("LeadId", data)}
                />

                <FormControl
                  control="date"
                  name="TaskEndDate"
                  label="Select Due Date"
                  setFun={(data) => {
                    return formik.setFieldValue("TaskEndDate", data);
                  }}
                />

                <FormControl
                  control="Priority"
                  name="Priority"
                  label="Select Priority"
                />

                <FormControl
                  control="users"
                  name="TaskOwners"
                  label="Add Users"
                  userName="userCc"
                  users={usersCc}
                  usersIdObj={usersIdObj}
                  setUsersIdObj={(data) => {
                    setUsersIdObj(data);
                  }}
                  setUsersCc={setUsersCc}
                  openAddUser={openAddCc}
                  setUserCount={(data) => {
                    formik.setFieldValue("TaskOwners", data);
                  }}
                  setOpenAddUser={(data) => setOpenAddCc(data)}
                />
              </div>

              <div className="buttonDiv">
                <Button className="btnAdd" id="Add" type="submit">
                  Add
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default forwardRef(AssignOthers);

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { POST_MY_TEAM } from "../api/apiEndPoints";
import { RequestAPi } from "../api/Request";

const initialState = {
  TeamMembers: [],
};

export const postGetTeams = createAsyncThunk(
  "postgetteams/postGetTeams",
  (data) => {
    return RequestAPi.post(POST_MY_TEAM, data).then((response) => response);
  }
);
const postGetTeamsSlice = createSlice({
  name: "postgetteams",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postGetTeams.fulfilled, (state, action) => {
      state.TeamMembers = action.payload.data.data.teamMembers;
    });
  },
});

export default postGetTeamsSlice.reducer;

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { COVERAGETASK } from "../api/apiEndPoints";
import { RequestAPi } from "../api/Request";

const initialState = {
  coverageData: "",
};


export const coverageTask = createAsyncThunk(
  "covearge/coverageTask",
  (data) => {
    return RequestAPi.get(`${COVERAGETASK}?taskId=${data.TaskId}`).then(
      (response) => response
    );
  }
);

const taskCoverageSlice = createSlice({
  name: "coverage",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(coverageTask.fulfilled, (state, action) => {
      state.coverageData = action.payload.data.data;
    });
  },
});

export default taskCoverageSlice.reducer;

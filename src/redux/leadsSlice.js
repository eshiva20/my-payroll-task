import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { GET_LEADS } from "../api/apiEndPoints";
import { RequestAPi } from "../api/Request";

const initialState = {
  localLeadData: [],
};

export const getLeads = createAsyncThunk("mytask/getLeads", (data) => {
  return RequestAPi.post(GET_LEADS, {
    From: 1,
    To: -1,
    Text: "",
  }).then((response) => response);
});
const leadsSlice = createSlice({
  name: "leads",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getLeads.fulfilled, (state, action) => {
      state.localLeadData = action.payload.data.data.Leads;
    });
  },
});

export default leadsSlice.reducer;

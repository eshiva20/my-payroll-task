import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { GET_MY_TASKS } from "../api/apiEndPoints";
import { RequestAPi } from "../api/Request";
import { toast } from "react-toastify";
import { toastobj } from "../utility/toastobj";

const initialState = {
  sendData: {
    TaskStatus: "",
    Priority: "",
    UserId: "",
    FromDueDate: "",
    ToDueDate: "",
    IsArchive: false,
    From: 1,
    To: 10,
    SortByDueDate: "",
    Title: "",
    UserIds: [],
    SortColumn: "",
    SortOrder: "",
  },
  isListFetching: false,
  difference: 10,
  toToDisplay: 1,
  localData: [],
  count: "",
  isPaginationLoading: false,
};

export const getMyTasks = createAsyncThunk("mytask/getMyTasks", (data) => {
  return RequestAPi.post(GET_MY_TASKS, data).then((response) => response);
});

const myTasksSlice = createSlice({
  name: "mytask",
  initialState,
  reducers: {
    setEndFromAndTo: (state) => {
      state.sendData.From = parseInt(state.count / state.difference) * 10 + 1;
      state.sendData.To = state.count;
    },
    setStartFromAndTo: (state) => {
      state.sendData.From = 1;
      state.sendData.To = state.difference;
    },
    setDifference: (state, action) => {
      state.difference = action.payload;
      if (state.sendData.From / state.difference >= 1) {
        state.sendData.From =
          state.difference *
            Math.floor(state.sendData.From / state.difference) +
          1;
      } else {
        state.sendData.From = 1;
      }
      state.sendData.To = state.sendData.From + state.difference - 1;
    },
    setFromAndTo: (state, action) => {
      if (action.payload.direction === "ltr") {
        state.sendData.From = state.sendData.To + 1;
        state.sendData.To = state.sendData.From + state.difference - 1;
      } else {
        if (Math.sign(state.sendData.From - 1 - state.difference) !== -1) {
          state.sendData.To = state.sendData.From - 1;
          state.sendData.From = state.sendData.From - state.difference;
        } else {
          state.sendData.To = state.sendData.From - 1;
          state.sendData.From = 1;
        }
      }
    },

    setSortData: (state, action) => {
      state.sendData.SortColumn = action.payload.column;
      state.sendData.SortOrder = action.payload.order;
    },

    setTitle: (state, action) => {
      state.sendData.Title = action.payload;
    },

    setSearch: (state, action) => {
      if (action.payload === "All") {
        state.sendData.FromDueDate = "";
        state.sendData.ToDueDate = "";
        state.sendData.Priority = "";
        state.sendData.TaskStatus = "";
      } else if (action.payload === "TaskStatus") {
        state.sendData.TaskStatus = "";
      } else if (action.payload === "Priority") {
        state.sendData.Priority = "";
      } else if (action.payload === "FromDueDate") {
        state.sendData.FromDueDate = "";
        state.sendData.ToDueDate = "";
      }
    },

    setSearchParams: (state, action) => {
      state.sendData.FromDueDate = action.payload.FromDueDate;
      state.sendData.ToDueDate = action.payload.ToDueDate;
      state.sendData.UserIds = action.payload.userIds;
      state.sendData.UserId = action.payload.UserId;
      state.sendData.Priority = action.payload.filterObject.Priority;
      state.sendData.TaskStatus = action.payload.filterObject.TaskStatus;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getMyTasks.pending, (state) => {
      state.isListFetching = true;
    });
    builder.addCase(getMyTasks.fulfilled, (state, action) => {
      state.isListFetching = false;
      const data = action.payload.data.data.TaskList;
      state.localData = data;
      state.count = action.payload.data.data.TotalCount;
    });
    builder.addCase(getMyTasks.rejected, (state) => {
      state.isListFetching = false;
      toast.error("Something Went Wrong", toastobj);
    });
  },
});

export default myTasksSlice.reducer;
export const {
  setFromAndTo,
  setDifference,
  setSortData,
  setTitle,
  setSearchParams,
  setEndFromAndTo,
  setStartFromAndTo,
  setSearch
} = myTasksSlice.actions;

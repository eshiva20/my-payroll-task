import { Route, Routes } from "react-router-dom";
import React, { lazy } from "react";
import MiniDrawer from "./components/TestSidebar";
import SignIn from "./components/SignIn";
import {
  colors,
  createTheme,
  ThemeProvider,
} from "@mui/material";
import { useState } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Dashboard = lazy(() => import("./components/Dashboard"));
const Settings = lazy(() => import("./components/Settings"));
const Mytask = lazy(() => import("./components/Mytask"));
const MyTeams = lazy(() => import("./components/MyTeams"));
const Billings = lazy(() => import("./components/Billings"));

const theme = createTheme({
  palette: {
    secondary: {
      main: colors.grey[100],
      light: colors.grey[500],
    },
    primary: {
      main: "#2979ff",
      light: colors.grey[500],
    },
    info: {
      main: colors.grey[100],
      light: "#9e9e9e",
    },
  },
  mixins: {
    toolbar: {
      minHeight: 46,
    },
  },
  shadows: [15],
});

function App() {
  const [selectedTitle, setSelectedTitle] = useState("");

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <ToastContainer />
        <Routes>
          <Route
            path="/"
            element={
              <MiniDrawer
                setSelectedTitle={setSelectedTitle}
                selectedTitle={selectedTitle}
              />
            }
          >
            <Route
              path="dashboard"
              element={<Dashboard setSelectedTitle={setSelectedTitle} />}
            />
            <Route
              path="mytasks"
              element={<Mytask setSelectedTitle={setSelectedTitle} />}
            />
            <Route
              path="myteams"
              element={<MyTeams setSelectedTitle={setSelectedTitle} />}
            />
            <Route
              path="billing"
              element={<Billings setSelectedTitle={setSelectedTitle} />}
            />
            <Route
              path="settings"
              element={<Settings setSelectedTitle={setSelectedTitle} />}
            />
          </Route>

          <Route path="login" element={<SignIn />} />
        </Routes>
      </div>
    </ThemeProvider>
  );
}

export default App;
